package docker

import (
	"fmt"
	"regexp"
	"strings"
)

const (
	rxHostDir         = `(?:\\\\\?\\)?[a-z]:[\\/](?:[^\\/:*?"<>|\r\n]+[\\/]?)*`
	rxName            = `[^\\/:*?"<>|\r\n]+`
	rxReservedNames   = `(con)|(prn)|(nul)|(aux)|(com[1-9])|(lpt[1-9])`
	rxPipe            = `[/\\]{2}.[/\\]pipe[/\\][^:*?"<>|\r\n]+`
	rxSource          = `((?P<source>((` + rxHostDir + `)|(` + rxName + `)|(` + rxPipe + `))):)?`
	rxDestination     = `(?P<destination>((?:\\\\\?\\)?([a-z]):((?:[\\/][^\\/:*?"<>\r\n]+)*[\\/]?))|(` + rxPipe + `))`
	rxLCOWDestination = `(?P<destination>/(?:[^\\/:*?"<>\r\n]+[/]?)*)`
	rxMode            = `(:(?P<mode>(?i)ro|rw))?`
)

// windowsSplitRawSpec handles splitting a volume spec for Windows where a colon
// is an acceptable character.
// This source is copied from the Moby source. Please review source for detailed
// comments on the above regex patterns.
// The fallthru error handling has been removed as that will be handled by the
// Docker engine.
// https://github.com/moby/moby/blob/master/volume/windows_parser.go
// commit e89b6e8c2d2c36c43f22aeaf2a885646c2994051
func windowsSplitRawSpec(raw string) ([]string, error) {
	specExp := regexp.MustCompile(`^` + rxSource + rxDestination + rxMode + `$`)
	match := specExp.FindStringSubmatch(strings.ToLower(raw))

	// Must have something back
	if len(match) == 0 {
		return nil, errInvalidSpec(raw)
	}

	var split []string
	matchgroups := make(map[string]string)
	// Pull out the sub expressions from the named capture groups
	for i, name := range specExp.SubexpNames() {
		matchgroups[name] = strings.ToLower(match[i])
	}
	if source, exists := matchgroups["source"]; exists {
		if source != "" {
			split = append(split, source)
		}
	}
	if destination, exists := matchgroups["destination"]; exists {
		if destination != "" {
			split = append(split, destination)
		}
	}
	if mode, exists := matchgroups["mode"]; exists {
		if mode != "" {
			split = append(split, mode)
		}
	}

	return split, nil
}

// https://github.com/moby/moby/blob/master/volume/volume.go
func errInvalidSpec(spec string) error {
	return fmt.Errorf("invalid volume specification: '%s'", spec)
}

func splitWindowsPath(raw string) ([]string, bool) {
	splits, err := windowsSplitRawSpec(raw)
	return splits, err == nil
}
